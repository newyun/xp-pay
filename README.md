# xp-pay

#### 项目介绍
xp-pay 是基于xposed的订单监控,支持支付宝和微信.
基于通知的 pxpay 请参考项目: [https://gitee.com/DaLianZhiYiKeJi/xpay](https://gitee.com/DaLianZhiYiKeJi/xpay)

#### 软件架构
xposed 切入下单接口.可以发起任意金额的订单.


#### 安装教程

1. 安装xposed
2. 安装微信/支付宝

